unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, System.Actions,
  Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, Vcl.Menus,
  System.ImageList, Vcl.ImgList, Vcl.StdCtrls, Vcl.ComCtrls, Clipbrd, AcsModule, Registry;

const MAX_BUFFER_LEN    = 256;

type
  TMainFrm = class(TForm)
    TrayIcon1: TTrayIcon;
    ImageList1: TImageList;
    PopupMenu1: TPopupMenu;
    X1: TMenuItem;
    N1: TMenuItem;
    X2: TMenuItem;
    ActionManager1: TActionManager;
    actExit: TAction;
    actShow: TAction;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    lbCardType: TLabel;
    Label2: TLabel;
    lbCardNo: TLabel;
    Button1: TButton;
    actCopyTo: TAction;
    Label3: TLabel;
    cbReader: TComboBox;
    Timer1: TTimer;
    cbHex: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure actShowExecute(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure actShowUpdate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actCopyToExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    m_bExit : boolean;
    procedure TypeKeyString(s: string);
  public
    { Public declarations }
  end;

var
  MainFrm: TMainFrm;
  hContext            : SCARDCONTEXT;
  hCard               : SCARDCONTEXT;
  ioRequest           : SCARD_IO_REQUEST;
  retCode             : Integer;
  dwActProtocol, BufferLen  : DWORD;
  SendBuff, RecvBuff        : array [0..262] of Byte;
  SendLen, RecvLen, nBytesRet : DWORD;
  Buffer      : array [0..MAX_BUFFER_LEN] of char;
  ATRVal              : array [1..36] of byte;
  ATRLen              : DWORD;
  RdrState    : SCARD_READERSTATE;
  PollStart     : boolean;

  lastCardNo : String;

implementation

{$R *.dfm}
procedure ClearBuffers();
var index: integer;
begin

  for index := 0 to 262 do
    begin
      SendBuff[index] := $00;
      RecvBuff[index] := $00;
    end;

end;

function GetCardId(): integer;
var
  index: integer;
  tempstr: String;
  cardId :Int64;
begin

  ClearBuffers();
  //Get operating parameters command
  SendBuff[0] := $FF;
  SendBuff[1] := $CA;
  SendBuff[2] := $00;
  SendBuff[3] := $00;
  SendBuff[4] := $00;

  SendLen := 5;
  RecvLen := 64;


  ioRequest.dwProtocol := dwActProtocol;
  ioRequest.cbPciLength := sizeof(SCARD_IO_REQUEST);

  tempstr := '';
  for index := 0 to SendLen - 1 do
    tempstr := tempstr + Format('%.02X ', [SendBuff[index]]);

  retCode := SCardTransmit(hCard,
                           @ioRequest,
                           @SendBuff,
                           SendLen,
                           Nil,
                           @RecvBuff,
                           @RecvLen);
  if retcode <> SCARD_S_SUCCESS then begin
    GetCardId := retcode;
    Exit;
  end;

  tempstr := '';
  cardId := 0;

  for index := 0 to RecvLen - 3 do
  begin
    tempstr := tempstr + Format('%.02X', [RecvBuff[index]]);
    cardId := cardId * 256 + RecvBuff[index];
  end;

  if cardId <> 0 then
  begin
    if MainFrm.cbHex.Checked then
    begin
      MainFrm.lbCardNo.Caption := tempstr;
    end
    else
    begin
      MainFrm.lbCardNo.Caption := IntToStr(cardId);
    end;

    if lastCardNo <> MainFrm.lbCardNo.Caption then
    begin
      MainFrm.TypeKeyString(MainFrm.lbCardNo.Caption);
      lastCardNo := MainFrm.lbCardNo.Caption;
    end;
  end;

  GetCardId := retcode;

end;

procedure InterpretATR();
var RIDVal, cardName: String;
    indx: Integer;
begin

  // 4. Interpret ATR and guess card
  // 4.1. Mifare cards using ISO 14443 Part 3 Supplemental Document
  if integer(ATRLen) > 14 then begin
    RIDVal := '';
    for indx := 8 to 12 do
      RIDVal := RIDVal + Format('%.02X',[ATRVal[indx]]);
    if (RIDVal = 'A000000306') then begin
      cardName := '';
      case ATRVal[13] of
        0: cardName := 'No card information';
        1: cardName := 'ISO 14443 A, Part1 Card Type';
        2: cardName := 'ISO 14443 A, Part2 Card Type';
        3: cardName := 'ISO 14443 A, Part3 Card Type';
        5: cardName := 'ISO 14443 B, Part1 Card Type';
        6: cardName := 'ISO 14443 B, Part2 Card Type';
        7: cardName := 'ISO 14443 B, Part3 Card Type';
        9: cardName := 'ISO 15693, Part1 Card Type';
        10: cardName := 'ISO 15693, Part2 Card Type';
        11: cardName := 'ISO 15693, Part3 Card Type';
        12: cardName := 'ISO 15693, Part4 Card Type';
        13: cardName := 'Contact Card (7816-10) IIC Card Type';
        14: cardName := 'Contact Card (7816-10) Extended IIC Card Type';
        15: cardName := 'Contact Card (7816-10) 2WBP Card Type';
        16: cardName := 'Contact Card (7816-10) 3WBP Card Type';
      else
        cardName := 'Undefined card';
      end;
      if (ATRVal[13] = $03) then
        if ATRVal[14] = $00 then
          case ATRVal[15] of
            $01: cardName := cardName + ': Mifare Standard 1K';
            $02: cardName := cardName + ': Mifare Standard 4K';
            $03: cardName := cardName + ': Mifare Ultra light';
            $04: cardName := cardName + ': SLE55R_XXXX';
            $06: cardName := cardName + ': SR176';
            $07: cardName := cardName + ': SRI X4K';
            $08: cardName := cardName + ': AT88RF020';
            $09: cardName := cardName + ': AT88SC0204CRF';
            $0A: cardName := cardName + ': AT88SC0808CRF';
            $0B: cardName := cardName + ': AT88SC1616CRF';
            $0C: cardName := cardName + ': AT88SC3216CRF';
            $0D: cardName := cardName + ': AT88SC6416CRF';
            $0E: cardName := cardName + ': SRF55V10P';
            $0F: cardName := cardName + ': SRF55V02P';
            $10: cardName := cardName + ': SRF55V10S';
            $11: cardName := cardName + ': SRF55V02S';
            $12: cardName := cardName + ': TAG IT';
            $13: cardName := cardName + ': LRI512';
            $14: cardName := cardName + ': ICODESLI';
            $15: cardName := cardName + ': TEMPSENS';
            $16: cardName := cardName + ': I.CODE1';
            $17: cardName := cardName + ': PicoPass 2K';
            $18: cardName := cardName + ': PicoPass 2KS';
            $19: cardName := cardName + ': PicoPass 16K';
            $1A: cardName := cardName + ': PicoPass 16KS';
            $1B: cardName := cardName + ': PicoPass 16K(8x2)';
            $1C: cardName := cardName + ': PicoPass 16KS(8x2)';

            $1D: cardName := cardName + ': PicoPass 32KS(16+16)';
            $1E: cardName := cardName + ': PicoPass 32KS(16+8x2)';
            $1F: cardName := cardName + ': PicoPass 32KS(8x2+16)';
            $20: cardName := cardName + ': PicoPass 32KS(8x2+8x2)';
            $21: cardName := cardName + ': LRI64';
            $22: cardName := cardName + ': I.CODE UID';
            $23: cardName := cardName + ': I.CODE EPC';
            $24: cardName := cardName + ': LRI12';
            $25: cardName := cardName + ': LRI128';
            $26: cardName := cardName + ': Mifare Mini';
          end
        else
          if ATRVal[14] = $FF then
            case ATRVal[14] of
              $09: cardName := cardName + ': Mifare Mini';
            end;
      MainFrm.lbCardType.Caption := cardname;
      //DisplayOut(cardName + ' is detected', 6);
    end;
   end;

  // 4.2. Mifare DESFire card using ISO 14443 Part 4
  if integer(ATRLen) > 11 then begin
    RIDVal := '';
    for indx := 5 to 10 do
      RIDVal := RIDVal + Format('%.02X',[ATRVal[indx]]);
    if (RIDVal = '067577810280') then
      MainFrm.lbCardType.Caption := 'Mifare DESFire';
      //DisplayOut('Mifare DESFire is detected', 6);
  end;

  // 4.3. Other cards using ISO 14443 Part 4
  if integer(ATRLen) > 17 then begin
    RIDVal := '';
    for indx := 5 to 16 do
      RIDVal := RIDVal + Format('%.02X',[ATRVal[indx]]);
    if (RIDVal = '50122345561253544E3381C3') then
      MainFrm.lbCardType.Caption := 'ST19XRC8E';
      //DisplayOut('ST19XRC8E is detected', 6);
  end;

end;

function CheckCard():boolean;
var ReaderLen: ^DWORD;
    dwState: ^DWORD;
    tempword: DWORD;
    index: integer;
begin

  //tempword := 32;
  //ATRLen := @tempword;
  //retCode := SCardStatusA(hCard,
  //                        PChar(frmPoll.cbReader.Text),
  //                        @ReaderLen,
  //                        @State,
  //                        @dwActProtocol,
  //                        @ATRVal,
  //                        @ATRLen);
  RdrState.szReader := PChar(MainFrm.cbReader.Text);
  retCode := SCardGetStatusChangeA(hContext,
                                       0,
                                       @RdrState,
                                       1);


  if retCode = SCARD_S_SUCCESS then begin
    if (RdrState.dwEventStates and SCARD_STATE_PRESENT) <> 0 then begin
      //if RdrState.rgbATR <> 0 then begin
        for index := 1 to 36 do
          ATRVal[index] := RdrState.rgbATR[index];
          ATRLen := Length(RdrState.rgbATR);
        InterpretATR();

        GetCardId;

        CheckCard := true;
      //end
      //else begin
      //  DisplayOut(GetScardErrMsg(retcode), 2);
      //  CheckCard := false;
      //end;
    end;
  end
  else begin
    CheckCard := false;
  end;

end;

procedure TMainFrm.actCopyToExecute(Sender: TObject);
begin
  Clipboard.AsText := lbCardNo.Caption;
end;

procedure TMainFrm.actExitExecute(Sender: TObject);
begin
  m_bExit := true;
  Close;
end;

procedure TMainFrm.actShowExecute(Sender: TObject);
begin
  if Self.Visible then
  begin
    Self.Hide;
  end
  else
  begin
    Self.Show;
  end;

end;

procedure TMainFrm.actShowUpdate(Sender: TObject);
begin
  if Self.Visible then
  begin
    actShow.Caption := '����������';
  end
  else
  begin
    actShow.Caption := '��ʾ������';
  end;
end;

procedure TMainFrm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if not m_bExit then
  begin
    Hide;
    Action := caNone;
  end;
end;

procedure TMainFrm.FormCreate(Sender: TObject);
var
  index: integer;
  reg:TRegistry;
begin
  reg := TRegistry.Create;
  reg.RootKey := HKEY_CURRENT_USER;
  if reg.OpenKey('Software\Childman\NfcReadId\',true) then
  begin
    try
      cbHex.Checked := reg.ReadBool('useHex');
    except on e:Exception do
      cbHex.Checked := false;
    end;
    reg.CloseKey;
  end;
  reg.Free;

  //Establish context
  retCode := SCardEstablishContext(SCARD_SCOPE_USER,
                                   nil,
                                   nil,
                                   @hContext);
  if retCode <> SCARD_S_SUCCESS then begin
    MessageBox(Self.Handle,PChar(GetScardErrMsg(retcode)),'����',MB_ICONERROR or MB_OK);
    Application.Terminate;
    Exit;
  end ;

  //List PC/SC readers installed in the system
  BufferLen := MAX_BUFFER_LEN;
  retCode := SCardListReadersA(hContext,
                               nil,
                               @Buffer,
                               @BufferLen);
  if retCode <> SCARD_S_SUCCESS then begin
    MessageBox(Self.Handle,PChar(GetScardErrMsg(retcode)),'����',MB_ICONERROR or MB_OK);
    Application.Terminate;
    Exit;
  end;

  LoadListToControl(cbReader,@buffer,bufferLen);
  // Look for ACR128 PICC and make it the default reader in the combobox
  for index := 0 to cbReader.Items.Count-1 do begin
    cbReader.ItemIndex := index;
    if AnsiPos('ACR122U PICC', cbReader.Text) > 0 then
      Exit;
  end;
  cbReader.ItemIndex := 0;

  //Connect to reader using a shared connection
//  retCode := SCardConnectA(hContext,
//                           PAnsiChar(cbReader.Text),
//                           SCARD_SHARE_SHARED,
//                           SCARD_PROTOCOL_T0 or SCARD_PROTOCOL_T1,
//                           @hCard,
//                           @dwActProtocol);
//
//  if retcode <> SCARD_S_SUCCESS then begin
////    MessageBox(Self.Handle,PChar(GetScardErrMsg(retcode)),'����',MB_ICONERROR or MB_OK);
////    Application.Terminate;
////    Exit;
//  end
//  else begin
//    StatusBar1.Panels[0].Text := cbReader.Text;
//  end;

  Timer1.Enabled := true;

end;

procedure TMainFrm.FormDestroy(Sender: TObject);
var
  reg:TRegistry;
begin
  reg := TRegistry.Create;
  reg.RootKey := HKEY_CURRENT_USER;
  if reg.OpenKey('Software\Childman\NfcReadId\',true) then
  begin
    try
      reg.WriteBool('useHex',cbHex.Checked);
    except on e:Exception do
      //cbHex.Checked := false;
    end;
    reg.CloseKey;
  end;
  reg.Free;
end;

// ģ���������һ���ַ���
procedure TMainFrm.Timer1Timer(Sender: TObject);
begin
  retCode := SCardConnectA(hContext,
                           PAnsiChar(cbReader.Text),
                           SCARD_SHARE_SHARED,
                           SCARD_PROTOCOL_T0 or SCARD_PROTOCOL_T1,
                           @hCard,
                           @dwActProtocol);

  if retcode <> SCARD_S_SUCCESS then begin
    StatusBar1.Panels[1].Text := '��ⷶΧ��û�п�';
    lastCardNo := '';
  end
  else begin
    if CheckCard = true then begin
    StatusBar1.Panels[1].Text := '��⵽��';
    //DisplayOut('Card is detected', 5);
    end
    else begin
    StatusBar1.Panels[1].Text := '��ⷶΧ��û�п�';
    //DisplayOut('No Card within range', 5);
    lastCardNo := '';
    end;
  end;
  retcode := ScardDisconnect(hCard, SCARD_UNPOWER_CARD);
end;

procedure TMainFrm.TypeKeyString(s: string);
var
  c: Char;
  i: integer;
  off: integer;
  vkw: Word;
begin
  for i := 1 to Length(s) do
  begin
    c := s[i];
    if (c < #128) then
    begin
      vkw := VkKeyScan(c);
      off := 0;
      if vkw and $100 = $100 then
        keybd_event(VK_SHIFT, 0, off, 0);
      if vkw and $200 = $200 then
        keybd_event(VK_CONTROL, 0, off, 0);
      if vkw and $400 = $400 then
        keybd_event(VK_MENU, 0, off, 0);

      off := 0;
      keybd_event(Byte(vkw), 0, off, 0);
      //sleep(20);
      off := off or KEYEVENTF_KEYUP;
      keybd_event(Byte(vkw), 0, off, 0);

      off := off or KEYEVENTF_KEYUP;
      if vkw and $100 = $100 then
        keybd_event(VK_SHIFT, 0, off, 0);
      if vkw and $200 = $200 then
        keybd_event(VK_CONTROL, 0, off, 0);
      if vkw and $400 = $400 then
        keybd_event(VK_MENU, 0, off, 0);
    end;
  end;
end;


end.
